/* 
 * File:   MXK.h
 * Author: David Ledger
 *
 * Created on 16 January 2017, 3:06 PM
 */
#ifndef MXK_H
#define	MXK_H
#include <stdbool.h>
#include "Types.h"
#include "Port.h"
#include "Config.h"
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
typedef enum
{
    eMXK_Motor  = MXK_MOTOR,
    eMXK_IMU    = MXK_IMU,
    eMXK_HMI    = MXK_HMI,
    eMXK_FPGA   = MXK_FPGA,
    eMXK_None   = 0
} eMXK_Module;
typedef enum 
{
    eMXK_Busy,
    eMXK_Idle
} eMXK_BusState;

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void            MXK_Init(void);
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
void            MXK_Poll(void);
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
bool            MXK_BlockSwitchTo(eMXK_Module pModule);
bool            MXK_SwitchTo(eMXK_Module pModule);
bool            MXK_Release();
void            MXK_Queue(Function pCallback);
void            MXK_Dequeue();
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////

////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////
#endif	/* MXK_H */

