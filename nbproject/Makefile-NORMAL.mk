#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Include project Makefile
ifeq "${IGNORE_LOCAL}" "TRUE"
# do not include local makefile. User is passing all local related variables already
else
include Makefile
# Include makefile containing local settings
ifeq "$(wildcard nbproject/Makefile-local-NORMAL.mk)" "nbproject/Makefile-local-NORMAL.mk"
include nbproject/Makefile-local-NORMAL.mk
endif
endif

# Environment
MKDIR=mkdir -p
RM=rm -f 
MV=mv 
CP=cp 

# Macros
CND_CONF=NORMAL
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
IMAGE_TYPE=debug
OUTPUT_SUFFIX=elf
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MX1_ASSIGNMENT2_A18_NAME_SIDxxxx.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
else
IMAGE_TYPE=production
OUTPUT_SUFFIX=hex
DEBUGGABLE_SUFFIX=elf
FINAL_IMAGE=dist/${CND_CONF}/${IMAGE_TYPE}/MX1_ASSIGNMENT2_A18_NAME_SIDxxxx.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}
endif

ifeq ($(COMPARE_BUILD), true)
COMPARISON_BUILD=--mafrlcsj
else
COMPARISON_BUILD=
endif

ifdef SUB_IMAGE_ADDRESS

else
SUB_IMAGE_ADDRESS_COMMAND=
endif

# Object Directory
OBJECTDIR=build/${CND_CONF}/${IMAGE_TYPE}

# Distribution Directory
DISTDIR=dist/${CND_CONF}/${IMAGE_TYPE}

# Source Files Quoted if spaced
SOURCEFILES_QUOTED_IF_SPACED=Library/USB/src/usb_device.c Library/USB/src/usb_device_cdc.c Library/USB/src/usb_descriptors.c Library/USB/src/usb_events.c Library/LCD.c Library/Point.c Library/Port.c Library/SPI.c Library/Types.c Library/FIFO.c Library/Console.c Library/Colours.c Library/MXK.c Library/system.c Library/USBWrap.c Library/Bitmap.c main.c

# Object Files Quoted if spaced
OBJECTFILES_QUOTED_IF_SPACED=${OBJECTDIR}/Library/USB/src/usb_device.p1 ${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1 ${OBJECTDIR}/Library/USB/src/usb_descriptors.p1 ${OBJECTDIR}/Library/USB/src/usb_events.p1 ${OBJECTDIR}/Library/LCD.p1 ${OBJECTDIR}/Library/Point.p1 ${OBJECTDIR}/Library/Port.p1 ${OBJECTDIR}/Library/SPI.p1 ${OBJECTDIR}/Library/Types.p1 ${OBJECTDIR}/Library/FIFO.p1 ${OBJECTDIR}/Library/Console.p1 ${OBJECTDIR}/Library/Colours.p1 ${OBJECTDIR}/Library/MXK.p1 ${OBJECTDIR}/Library/system.p1 ${OBJECTDIR}/Library/USBWrap.p1 ${OBJECTDIR}/Library/Bitmap.p1 ${OBJECTDIR}/main.p1
POSSIBLE_DEPFILES=${OBJECTDIR}/Library/USB/src/usb_device.p1.d ${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1.d ${OBJECTDIR}/Library/USB/src/usb_descriptors.p1.d ${OBJECTDIR}/Library/USB/src/usb_events.p1.d ${OBJECTDIR}/Library/LCD.p1.d ${OBJECTDIR}/Library/Point.p1.d ${OBJECTDIR}/Library/Port.p1.d ${OBJECTDIR}/Library/SPI.p1.d ${OBJECTDIR}/Library/Types.p1.d ${OBJECTDIR}/Library/FIFO.p1.d ${OBJECTDIR}/Library/Console.p1.d ${OBJECTDIR}/Library/Colours.p1.d ${OBJECTDIR}/Library/MXK.p1.d ${OBJECTDIR}/Library/system.p1.d ${OBJECTDIR}/Library/USBWrap.p1.d ${OBJECTDIR}/Library/Bitmap.p1.d ${OBJECTDIR}/main.p1.d

# Object Files
OBJECTFILES=${OBJECTDIR}/Library/USB/src/usb_device.p1 ${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1 ${OBJECTDIR}/Library/USB/src/usb_descriptors.p1 ${OBJECTDIR}/Library/USB/src/usb_events.p1 ${OBJECTDIR}/Library/LCD.p1 ${OBJECTDIR}/Library/Point.p1 ${OBJECTDIR}/Library/Port.p1 ${OBJECTDIR}/Library/SPI.p1 ${OBJECTDIR}/Library/Types.p1 ${OBJECTDIR}/Library/FIFO.p1 ${OBJECTDIR}/Library/Console.p1 ${OBJECTDIR}/Library/Colours.p1 ${OBJECTDIR}/Library/MXK.p1 ${OBJECTDIR}/Library/system.p1 ${OBJECTDIR}/Library/USBWrap.p1 ${OBJECTDIR}/Library/Bitmap.p1 ${OBJECTDIR}/main.p1

# Source Files
SOURCEFILES=Library/USB/src/usb_device.c Library/USB/src/usb_device_cdc.c Library/USB/src/usb_descriptors.c Library/USB/src/usb_events.c Library/LCD.c Library/Point.c Library/Port.c Library/SPI.c Library/Types.c Library/FIFO.c Library/Console.c Library/Colours.c Library/MXK.c Library/system.c Library/USBWrap.c Library/Bitmap.c main.c


CFLAGS=
ASFLAGS=
LDLIBSOPTIONS=

############# Tool locations ##########################################
# If you copy a project from one host to another, the path where the  #
# compiler is installed may be different.                             #
# If you open this project with MPLAB X in the new host, this         #
# makefile will be regenerated and the paths will be corrected.       #
#######################################################################
# fixDeps replaces a bunch of sed/cat/printf statements that slow down the build
FIXDEPS=fixDeps

.build-conf:  ${BUILD_SUBPROJECTS}
ifneq ($(INFORMATION_MESSAGE), )
	@echo $(INFORMATION_MESSAGE)
endif
	${MAKE}  -f nbproject/Makefile-NORMAL.mk dist/${CND_CONF}/${IMAGE_TYPE}/MX1_ASSIGNMENT2_A18_NAME_SIDxxxx.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}

MP_PROCESSOR_OPTION=18F67J50
# ------------------------------------------------------------------------------------
# Rules for buildStep: compile
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
${OBJECTDIR}/Library/USB/src/usb_device.p1: Library/USB/src/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library/USB/src" 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_device.p1.d 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_device.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/USB/src/usb_device.p1  Library/USB/src/usb_device.c 
	@-${MV} ${OBJECTDIR}/Library/USB/src/usb_device.d ${OBJECTDIR}/Library/USB/src/usb_device.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/USB/src/usb_device.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1: Library/USB/src/usb_device_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library/USB/src" 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1.d 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1  Library/USB/src/usb_device_cdc.c 
	@-${MV} ${OBJECTDIR}/Library/USB/src/usb_device_cdc.d ${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/USB/src/usb_descriptors.p1: Library/USB/src/usb_descriptors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library/USB/src" 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_descriptors.p1.d 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_descriptors.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/USB/src/usb_descriptors.p1  Library/USB/src/usb_descriptors.c 
	@-${MV} ${OBJECTDIR}/Library/USB/src/usb_descriptors.d ${OBJECTDIR}/Library/USB/src/usb_descriptors.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/USB/src/usb_descriptors.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/USB/src/usb_events.p1: Library/USB/src/usb_events.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library/USB/src" 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_events.p1.d 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_events.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/USB/src/usb_events.p1  Library/USB/src/usb_events.c 
	@-${MV} ${OBJECTDIR}/Library/USB/src/usb_events.d ${OBJECTDIR}/Library/USB/src/usb_events.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/USB/src/usb_events.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/LCD.p1: Library/LCD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/LCD.p1.d 
	@${RM} ${OBJECTDIR}/Library/LCD.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/LCD.p1  Library/LCD.c 
	@-${MV} ${OBJECTDIR}/Library/LCD.d ${OBJECTDIR}/Library/LCD.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/LCD.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Point.p1: Library/Point.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Point.p1.d 
	@${RM} ${OBJECTDIR}/Library/Point.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Point.p1  Library/Point.c 
	@-${MV} ${OBJECTDIR}/Library/Point.d ${OBJECTDIR}/Library/Point.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Point.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Port.p1: Library/Port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Port.p1.d 
	@${RM} ${OBJECTDIR}/Library/Port.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Port.p1  Library/Port.c 
	@-${MV} ${OBJECTDIR}/Library/Port.d ${OBJECTDIR}/Library/Port.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Port.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/SPI.p1: Library/SPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/SPI.p1.d 
	@${RM} ${OBJECTDIR}/Library/SPI.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/SPI.p1  Library/SPI.c 
	@-${MV} ${OBJECTDIR}/Library/SPI.d ${OBJECTDIR}/Library/SPI.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/SPI.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Types.p1: Library/Types.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Types.p1.d 
	@${RM} ${OBJECTDIR}/Library/Types.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Types.p1  Library/Types.c 
	@-${MV} ${OBJECTDIR}/Library/Types.d ${OBJECTDIR}/Library/Types.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Types.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/FIFO.p1: Library/FIFO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/FIFO.p1.d 
	@${RM} ${OBJECTDIR}/Library/FIFO.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/FIFO.p1  Library/FIFO.c 
	@-${MV} ${OBJECTDIR}/Library/FIFO.d ${OBJECTDIR}/Library/FIFO.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/FIFO.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Console.p1: Library/Console.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Console.p1.d 
	@${RM} ${OBJECTDIR}/Library/Console.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Console.p1  Library/Console.c 
	@-${MV} ${OBJECTDIR}/Library/Console.d ${OBJECTDIR}/Library/Console.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Console.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Colours.p1: Library/Colours.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Colours.p1.d 
	@${RM} ${OBJECTDIR}/Library/Colours.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Colours.p1  Library/Colours.c 
	@-${MV} ${OBJECTDIR}/Library/Colours.d ${OBJECTDIR}/Library/Colours.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Colours.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/MXK.p1: Library/MXK.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/MXK.p1.d 
	@${RM} ${OBJECTDIR}/Library/MXK.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/MXK.p1  Library/MXK.c 
	@-${MV} ${OBJECTDIR}/Library/MXK.d ${OBJECTDIR}/Library/MXK.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/MXK.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/system.p1: Library/system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/system.p1.d 
	@${RM} ${OBJECTDIR}/Library/system.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/system.p1  Library/system.c 
	@-${MV} ${OBJECTDIR}/Library/system.d ${OBJECTDIR}/Library/system.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/system.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/USBWrap.p1: Library/USBWrap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/USBWrap.p1.d 
	@${RM} ${OBJECTDIR}/Library/USBWrap.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/USBWrap.p1  Library/USBWrap.c 
	@-${MV} ${OBJECTDIR}/Library/USBWrap.d ${OBJECTDIR}/Library/USBWrap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/USBWrap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Bitmap.p1: Library/Bitmap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Bitmap.p1.d 
	@${RM} ${OBJECTDIR}/Library/Bitmap.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Bitmap.p1  Library/Bitmap.c 
	@-${MV} ${OBJECTDIR}/Library/Bitmap.d ${OBJECTDIR}/Library/Bitmap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Bitmap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/main.p1  main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
else
${OBJECTDIR}/Library/USB/src/usb_device.p1: Library/USB/src/usb_device.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library/USB/src" 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_device.p1.d 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_device.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/USB/src/usb_device.p1  Library/USB/src/usb_device.c 
	@-${MV} ${OBJECTDIR}/Library/USB/src/usb_device.d ${OBJECTDIR}/Library/USB/src/usb_device.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/USB/src/usb_device.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1: Library/USB/src/usb_device_cdc.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library/USB/src" 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1.d 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1  Library/USB/src/usb_device_cdc.c 
	@-${MV} ${OBJECTDIR}/Library/USB/src/usb_device_cdc.d ${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/USB/src/usb_device_cdc.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/USB/src/usb_descriptors.p1: Library/USB/src/usb_descriptors.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library/USB/src" 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_descriptors.p1.d 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_descriptors.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/USB/src/usb_descriptors.p1  Library/USB/src/usb_descriptors.c 
	@-${MV} ${OBJECTDIR}/Library/USB/src/usb_descriptors.d ${OBJECTDIR}/Library/USB/src/usb_descriptors.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/USB/src/usb_descriptors.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/USB/src/usb_events.p1: Library/USB/src/usb_events.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library/USB/src" 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_events.p1.d 
	@${RM} ${OBJECTDIR}/Library/USB/src/usb_events.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/USB/src/usb_events.p1  Library/USB/src/usb_events.c 
	@-${MV} ${OBJECTDIR}/Library/USB/src/usb_events.d ${OBJECTDIR}/Library/USB/src/usb_events.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/USB/src/usb_events.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/LCD.p1: Library/LCD.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/LCD.p1.d 
	@${RM} ${OBJECTDIR}/Library/LCD.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/LCD.p1  Library/LCD.c 
	@-${MV} ${OBJECTDIR}/Library/LCD.d ${OBJECTDIR}/Library/LCD.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/LCD.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Point.p1: Library/Point.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Point.p1.d 
	@${RM} ${OBJECTDIR}/Library/Point.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Point.p1  Library/Point.c 
	@-${MV} ${OBJECTDIR}/Library/Point.d ${OBJECTDIR}/Library/Point.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Point.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Port.p1: Library/Port.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Port.p1.d 
	@${RM} ${OBJECTDIR}/Library/Port.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Port.p1  Library/Port.c 
	@-${MV} ${OBJECTDIR}/Library/Port.d ${OBJECTDIR}/Library/Port.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Port.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/SPI.p1: Library/SPI.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/SPI.p1.d 
	@${RM} ${OBJECTDIR}/Library/SPI.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/SPI.p1  Library/SPI.c 
	@-${MV} ${OBJECTDIR}/Library/SPI.d ${OBJECTDIR}/Library/SPI.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/SPI.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Types.p1: Library/Types.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Types.p1.d 
	@${RM} ${OBJECTDIR}/Library/Types.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Types.p1  Library/Types.c 
	@-${MV} ${OBJECTDIR}/Library/Types.d ${OBJECTDIR}/Library/Types.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Types.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/FIFO.p1: Library/FIFO.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/FIFO.p1.d 
	@${RM} ${OBJECTDIR}/Library/FIFO.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/FIFO.p1  Library/FIFO.c 
	@-${MV} ${OBJECTDIR}/Library/FIFO.d ${OBJECTDIR}/Library/FIFO.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/FIFO.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Console.p1: Library/Console.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Console.p1.d 
	@${RM} ${OBJECTDIR}/Library/Console.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Console.p1  Library/Console.c 
	@-${MV} ${OBJECTDIR}/Library/Console.d ${OBJECTDIR}/Library/Console.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Console.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Colours.p1: Library/Colours.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Colours.p1.d 
	@${RM} ${OBJECTDIR}/Library/Colours.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Colours.p1  Library/Colours.c 
	@-${MV} ${OBJECTDIR}/Library/Colours.d ${OBJECTDIR}/Library/Colours.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Colours.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/MXK.p1: Library/MXK.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/MXK.p1.d 
	@${RM} ${OBJECTDIR}/Library/MXK.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/MXK.p1  Library/MXK.c 
	@-${MV} ${OBJECTDIR}/Library/MXK.d ${OBJECTDIR}/Library/MXK.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/MXK.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/system.p1: Library/system.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/system.p1.d 
	@${RM} ${OBJECTDIR}/Library/system.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/system.p1  Library/system.c 
	@-${MV} ${OBJECTDIR}/Library/system.d ${OBJECTDIR}/Library/system.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/system.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/USBWrap.p1: Library/USBWrap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/USBWrap.p1.d 
	@${RM} ${OBJECTDIR}/Library/USBWrap.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/USBWrap.p1  Library/USBWrap.c 
	@-${MV} ${OBJECTDIR}/Library/USBWrap.d ${OBJECTDIR}/Library/USBWrap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/USBWrap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/Library/Bitmap.p1: Library/Bitmap.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}/Library" 
	@${RM} ${OBJECTDIR}/Library/Bitmap.p1.d 
	@${RM} ${OBJECTDIR}/Library/Bitmap.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/Library/Bitmap.p1  Library/Bitmap.c 
	@-${MV} ${OBJECTDIR}/Library/Bitmap.d ${OBJECTDIR}/Library/Bitmap.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/Library/Bitmap.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
${OBJECTDIR}/main.p1: main.c  nbproject/Makefile-${CND_CONF}.mk
	@${MKDIR} "${OBJECTDIR}" 
	@${RM} ${OBJECTDIR}/main.p1.d 
	@${RM} ${OBJECTDIR}/main.p1 
	${MP_CC} --pass1 $(MP_EXTRA_CC_PRE) --chip=$(MP_PROCESSOR_OPTION) -Q -G  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib $(COMPARISON_BUILD)  --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"    -o${OBJECTDIR}/main.p1  main.c 
	@-${MV} ${OBJECTDIR}/main.d ${OBJECTDIR}/main.p1.d 
	@${FIXDEPS} ${OBJECTDIR}/main.p1.d $(SILENT) -rsi ${MP_CC_DIR}../  
	
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: assemble
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
else
endif

# ------------------------------------------------------------------------------------
# Rules for buildStep: link
ifeq ($(TYPE_IMAGE), DEBUG_RUN)
dist/${CND_CONF}/${IMAGE_TYPE}/MX1_ASSIGNMENT2_A18_NAME_SIDxxxx.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk    
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/MX1_ASSIGNMENT2_A18_NAME_SIDxxxx.X.${IMAGE_TYPE}.map  -D__DEBUG=1 --debugger=none  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"        $(COMPARISON_BUILD) --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/MX1_ASSIGNMENT2_A18_NAME_SIDxxxx.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	@${RM} dist/${CND_CONF}/${IMAGE_TYPE}/MX1_ASSIGNMENT2_A18_NAME_SIDxxxx.X.${IMAGE_TYPE}.hex 
	
else
dist/${CND_CONF}/${IMAGE_TYPE}/MX1_ASSIGNMENT2_A18_NAME_SIDxxxx.X.${IMAGE_TYPE}.${OUTPUT_SUFFIX}: ${OBJECTFILES}  nbproject/Makefile-${CND_CONF}.mk   
	@${MKDIR} dist/${CND_CONF}/${IMAGE_TYPE} 
	${MP_CC} $(MP_EXTRA_LD_PRE) --chip=$(MP_PROCESSOR_OPTION) -G -mdist/${CND_CONF}/${IMAGE_TYPE}/MX1_ASSIGNMENT2_A18_NAME_SIDxxxx.X.${IMAGE_TYPE}.map  --double=32 --float=32 --emi=wordwrite --opt=+asm,+asmfile,-speed,+space,-debug,-local --addrqual=ignore --mode=std -P -N255 -I"../" -I"./" -I"Library/USB/inc" -I"Library/USB/src" -I"Library/USB" -I"Library" --warn=-3 --asmlist -DXPRJ_NORMAL=$(CND_CONF)  --summary=default,-psect,-class,+mem,-hex,-file --output=default,-inhx032 --runtime=default,+clear,+init,-keep,-no_startup,-download,+config,+clib,-plib --output=-mcof,+elf:multilocs --stack=compiled:auto:auto:auto "--errformat=%f:%l: error: (%n) %s" "--warnformat=%f:%l: warning: (%n) %s" "--msgformat=%f:%l: advisory: (%n) %s"     $(COMPARISON_BUILD) --memorysummary dist/${CND_CONF}/${IMAGE_TYPE}/memoryfile.xml -odist/${CND_CONF}/${IMAGE_TYPE}/MX1_ASSIGNMENT2_A18_NAME_SIDxxxx.X.${IMAGE_TYPE}.${DEBUGGABLE_SUFFIX}  ${OBJECTFILES_QUOTED_IF_SPACED}     
	
endif


# Subprojects
.build-subprojects:


# Subprojects
.clean-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r build/NORMAL
	${RM} -r dist/NORMAL

# Enable dependency checking
.dep.inc: .depcheck-impl

DEPFILES=$(shell "${PATH_TO_IDE_BIN}"mplabwildcard ${POSSIBLE_DEPFILES})
ifneq (${DEPFILES},)
include ${DEPFILES}
endif
