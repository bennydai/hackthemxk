/*
 * Assignment Title:
 * Name:
 * Student No:
 * Email:
 *
 * File:   main.c
 * Due Date:
 *
 * Useful MXK Information:
 * The CPU Clock Speed is 12MHz
 * Schematics are avaliable on MXKOnline
 *
 */

//REQUIRED LIBRARIES - Theses are the standard libraries to allow the XC8 Complier to understand our commands and to allow MXK interfacing
#include <xc.h>
#include <stdio.h>
#include <string.h>
#include "ProcessorConfig.h"
#include "MXK.h"

//APPLICATION LIBRARIES - Libraries used for the application
#include "LCD.h"
#include "Console.h"
#include "USBWrap.h"

//YOUR GLOBAL VARIABLES
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//

//FUNCTIONS
void motorTimerSetup() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void motorTimerStart() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void motorTimerStop() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void fadeTimerSetup() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void fadeTimerStart() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void fadeTimerStop() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void pwmSetup() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void pwmStart() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void pwmStop() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void pwmFaderTimerCheck() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void motorSingleStep(char Direction) { // this function should step the motor once and keep track of its state and how many steps it has been driven
    if (MXK_BlockSwitchTo(eMXK_Motor)) {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
    }
    if (MXK_Release()) {
        MXK_Dequeue();
    }

}

void motorTimerCheck() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void motorMove(char Direction, int Steps, int StepPeriod) { // this function should start the stepper motors movement (You will need global variables))
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void motorInit() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

void adcInit() {
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}


// you must fill out the missing details for this function, if you can't, remove this section
//--- adcRead(--- channel) {  
//------------------------------ENTER YOU CODE BELOW------------------------------------//

//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
//}
//---------------------------------END OF SECTION---------------------------------------//

/*
 * LCD SECTION:
 * DESIGN AND WRITE CODE THAT WILL DISPLAY THE FOLLOWING LAYOUT. NOTE: THE COLOURS IN BRACKETS ARE THE COLOURS THE LINES SHOULD BE.
 * BACKGROUND: CENTERED MAGENTA CIRCLE
 * ------------------------------------------------------------
 * |Mechatronics 1 (Green)                                    |
 * |Assignment 2 A2018 (Green)                                |
 * |[Your Full Name] (White)                                  |
 * |[Your Student Number] (White)                             |
 * |ADC CH0 = [Current ADC Channel 0 Value] (Yellow)          |
 * |ADC CH1 = [Current ADC Channel 1 Value] (Yellow)          |
 * |Motor Steps = [Current Motor Steps] (Grey)                 |
 * |Request Steps = [Next Motor Steps] (Cyan)                 |
 * |Request Speed = [Next Motor Speed] (Cyan)                 |
 * |Request Direction = [Next Motor Direction (CW/CCw)] (Cyan)| 
 * ------------------------------------------------------------
 * NOTE: FOR THE PROVIDED NEMA 17 STEPPER MOTOR, IT HAS 200 STEPS BEFORE IT DOES 1 REV. THEREFORE YOUR STEPS SHOULD GO
 * FROM 0-199 AND BACK TO 0, I.E. 0->1..199->0->1..199.
 *
 */
void lcdInit() {
    if (MXK_BlockSwitchTo(eMXK_HMI)) { //Switches MXK multiplexing system to HMI module
//------------------------------ENTER YOU CODE BELOW------------------------------------//
        LCD_Reset();
        LCD_Init();
        Console_Init();
        
       
//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
    }
    if (MXK_Release()) {
        MXK_Dequeue();
    }
}

void lcdDisplay(char *sentences) {
    if (MXK_SwitchTo(eMXK_HMI)) { //Switches MXK multiplexing system to HMI module only if not being used by interrupt
//------------------------------ENTER YOU CODE BELOW------------------------------------//
        printf(sentences);
        Console_SetBackcolour(BLACK);
        Console_Render();   
//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
    }
    if (MXK_Release()) {
        MXK_Dequeue();
    }
}

void BluetoothInit(){
    
    BAUDCON1bits.BRG16 = 1; //set the baud rate according to formula
    TXSTA1bits.BRGH = 1; //set the register on
    
    SPBRGH1 = 0x04; //FOSC is 48
    SPBRG1 = 0xE1; // Using the formula we get these two values in order to satisfy a baud rate of 9600.
    
    TXSTA1bits.SYNC = 0; //turn off sync
    RCSTA1bits.SPEN = 1; 
    
    TRISCbits.RC6=1;
    TRISCbits.RC7=1;
    
    INTCONbits.GIE = 1;
    INTCONbits.PEIE = 1;

    PIE1bits.TX1IE = 1;
    PIE1bits.RC1IE =1;
    
    TXSTAbits.TXEN = 1;
    
    RCSTA1bits.RX9 = 0;
    RCSTAbits.CREN = 1;
    
    TXSTA1bits.TX9 = 0;
    
    lcdDisplay("Bluetooth Inititiated\n");
}

void BT_load_char(char byte){
    while(!PIR1bits.TX1IF);
    TXREG1 = byte;
}

void BT_load_string(char *string){
    while(*string)
    {
        BT_load_char(*string++);
    }
}

void broadcast_BT(){
    TXREG1 = 0x0A; // Transmits the ascii new line character
}

char BT_get_char(void){
    if(RCSTAbits.OERR) //prevents overrun
    {
        RCSTA1bits.CREN = 0;
        RCSTA1bits.CREN = 1; //Reset CREN
    }
    while(!PIR1bits.RC1IF); //until 
    return(RCREG1);
}

char BT_get_string(void){
    char get_reading[16];
    char *placeholder;
    char *filter = "\r";
        int i = 0;
    for (int i = 0; i < 16; i++)
    {
        get_reading[i] = BT_get_char();
        delay_ms(1);
        if (get_reading[i] == 10 || get_reading[i] == 13)
        {
            placeholder = strtok(get_reading, filter);
            return placeholder;
        }
    }
}

void Clear_String(char *string){
    memset(string, 0, sizeof(string));
}

//MAIN FUNCTION
void main() {
    char buffer[];
    char *pbuffer;
    pbuffer = &buffer;
    MXK_Init();
    lcdInit();
    BluetoothInit();
    lcdDisplay("Setup Finished\n");
    BT_load_string("Setup Finished\n");
    while(1)
    {
        pbuffer = BT_get_string();
        delay_ms(100);
        BT_load_string(pbuffer);
        delay_ms(100);
        lcdDisplay(buffer);
        Clear_String(pbuffer);
        /*if (get_reading == '1')
        {
            BT_load_string("Gavin give me marks please");
            delay_ms(1);
            broadcast_BT();
        }
        
        if (get_reading == '0')
        {
            BT_load_string("This is communicated via bluetooth");
            delay_ms(1);
            broadcast_BT();
        }*/   
    }   
//------------------------------ENTER YOU CODE BELOW------------------------------------//
    
//------------------------------FINISH YOUR CODE ABOVE----------------------------------//
}

//END OF BLANK TEMPLATE


//FOR ADVANCED STUDENTS ONLY
//interrupt function - feel free to use this, but at this stage of the course 
//                     we do not expect you to use it. If you do, this will 
//                     be an advantage to you when you move on to MX2

//void interrupt isr() {
//
//}


